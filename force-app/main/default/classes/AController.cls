public class AController {
    @AuraEnabled
    public String name;
    @AuraEnabled
    public DateTime fileDate;
    @AuraEnabled
    public String fileId;
    @AuraEnabled
    public String fileType;
    @AuraEnabled
    public   List<String> commentList  = new List<String>();
    @AuraEnabled
    public   List<String> attList  = new List<String>();
     @AuraEnabled
    public   List<String> emailList  = new List<String>();
    
    
    
    @AuraEnabled
    public static List<Case> getmain() {
        return [select Id,CaseNumber,ContactEmail,Subject,Origin,Reason,status  from Case ];
    }
    @AuraEnabled
    public static List<Case> getmain1(string idOfRecord) {
   system.debug('asas' +idOfRecord);
        return [Select id ,subject,Product__c,AccountId,LastViewedDate ,Status,EngineeringReqNumber__c,CaseNumber,ContactEmail,Reason,Origin From Case where id =:idOfRecord];
   
    }
     @AuraEnabled
    public static Case updateStatus(String status) {
        Case snew=[Select id ,subject,Product__c,AccountId,LastViewedDate ,Status,EngineeringReqNumber__c,CaseNumber,ContactEmail,Reason,Origin From Case where id =:'5002v00002M7AzeAAF'] ;
        snew.Status=status;
        upsert snew;
        return snew;
    }
    
    
    @AuraEnabled
    public static list <CaseComment> getCaseComment(string idOfRecord){
        
        List<CaseComment> commentList = [Select Id, ParentId, IsPublished, CommentBody, CreatedBy.name,
                                         CreatedDate, SystemModstamp, LastModifiedDate, LastModifiedById,
                                         IsDeleted From CaseComment where ParentId=:idOfRecord];
        return commentList;
    }
    @AuraEnabled
    public static list <Attachment> getAtt(string idOfRecord){
        
        List<Attachment> attList = [select Id, Name, ContentType,CreatedDate  from Attachment  where ParentId=:idOfRecord];
        return attList;
    }
     @AuraEnabled
    public static list <EmailMessage > getEmails(){
        
        List<EmailMessage >emailList = [SELECT Id, ParentId, ToAddress , TextBody,HTMLBody, FromAddress, Subject, FromName FROM EmailMessage where ParentId='5002v00002M7AzkAAF'];
        return emailList;
    }
     @AuraEnabled
      public static List<String> getCategory(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
        
    @AuraEnabled
    public static list <CaseComment> createCaseComment(String parentId, String newtext){
        
        CaseComment c= new  CaseComment();
        c.ParentId=parentId;
        c.CommentBody=newtext;
        insert c;
      List<CaseComment> commentList = [Select Id, ParentId, IsPublished, CommentBody, CreatedBy.name,
                                         CreatedDate, SystemModstamp, LastModifiedDate, LastModifiedById,
                                         IsDeleted From CaseComment where ParentId='5002v00002M7AzkAAF'];
        return commentList;
    
    
    
    }
   
    
    @AuraEnabled
    public  Case getstatus() {
        Case obj=[select Status from Case where id =:'5002v00002M7AzeAAF'];
        obj.status='Open';
        update obj;
        return obj;
    }
     @AuraEnabled 
    public static list<EmailMessage> sendMailMethod(String mMail ,String mSubject ,String mbody){
    
     List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();     
  
     // Step 1: Create a new Email
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setWhatId('5002v00002M7AzkAAF');
        
      
     
    
    // Step 2: Set list of people who should get the email
       List<String> sendTo = new List<String>();
       sendTo.add(mMail);
       mail.setToAddresses(sendTo);
    
    // Step 3: Set who the email is sent from
       mail.setReplyTo('noreply@gmail.com'); // change it with your mail address.
       mail.setSenderDisplayName('salesforce User'); 
    
    // Step 4. Set email contents - you can use variables!
      mail.setSubject(mSubject);
      mail.setHtmlBody(mbody);
    
    // Step 5. Add your email to the master list
      mails.add(mail);
    
  // Step 6: Send all emails in the master list
     Messaging.sendEmail(mails);
          
        List<EmailMessage >emailList = [SELECT Id, ParentId, ToAddress , TextBody,HTMLBody, FromAddress, Subject, FromName FROM EmailMessage where ParentId='5002v00002M7AzkAAF'];
        return emailList; 
            } 
        @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        //  which is save the check data and return the attachemnt Id after insert, 
        //  next time (in else) we are call the appentTOFile() method
        //   for update the attachment with reamins chunks   
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
 
        return Id.valueOf(fileId);
    }
 
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
        Attachment oAttachment = new Attachment();
        oAttachment.parentId = '5002v00002M7AzkAAF';
 
        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        oAttachment.Name = fileName;
        oAttachment.ContentType = contentType;
 
        insert oAttachment;
 
        return oAttachment.Id;
    }
 
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id =: fileId
        ];
 
        String existingBody = EncodingUtil.base64Encode(a.Body);
 
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);
 
        update a;
    }
    @AuraEnabled  
    public static void deleteFiles(string sdocumentId){ 
        delete [select Id, Name, ContentType,CreatedDate  from Attachment WHERE id=:sdocumentId];       
    } 
    
    
    
}